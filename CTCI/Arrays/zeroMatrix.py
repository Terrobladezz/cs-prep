inputMatrix = [
    [1, 2, 3, 4, 0],
    [0, 2, 3, 4, 1],
    [1, 2, 3, 4, 5],
    [5, 4, 3, 2, 1],
    [5, 7, 3, 2, 1]
]


# 0 0 0 0 0
# 0 0 0 0 0
# 0 2 3 4 0
# 0 4 3 2 0
# 0 7 3 2 1

def zeroMatrix(arr):
    indexPairs = []
    for i in range(len(arr)):
        for j in range(len(arr)):
            if arr[i][j] == 0:
                indexPairs.append([i, j])
    for items in indexPairs:
        for j in range(len(arr)):
            arr[items[0]][j] = 0
        for i in range(len(arr)):
            arr[i][items[1]] = 0

    for item in arr:
        print(item)


zeroMatrix(inputMatrix)
