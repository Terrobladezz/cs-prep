# insert , remove or replace

# ex : pale , ple
def oneAway(str1,str2):
    if abs(len(str1) - len(str2)) > 1 :
        return False

    one = False
    if len(str1) > len(str2):
        big = str1
        small = str2
    else:
        big = str2
        small = str1

    for i in big :
        if i not in small and not one:
            one = True
        elif i not in small :
            return False
    return True

print(oneAway('pale' , 'ple'))
print(oneAway('pales' , 'pale'))
print(oneAway('pale' , 'bale'))
print(oneAway('pale' , 'bake'))


