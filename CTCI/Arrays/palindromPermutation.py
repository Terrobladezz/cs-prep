def palindromePermutation(data):
    dict  = {}

    data = data.replace(' ','')
    for i in data : 
        if i in dict:
            dict[i] +=1
        else : 
            dict[i] = 1
    
    values = dict.values()
    if len(data)%2 == 0 : 
        for i in values : 
            if i%2 != 0 : 
                return False
        return True
    else  :
        oneOdd = False
        for i in values  :

            if i%2 !=0 and oneOdd : 
                return False
            elif i%2!=0 and not oneOdd:
                oneOdd = True
        return True

print(palindromePermutation('taco cat'))
print(palindromePermutation('atco cat'))
print(palindromePermutation('aabc'))
print(palindromePermutation('aaaabbbbcc'))
print(palindromePermutation(''))
print(palindromePermutation('chirpingmermaid'))





        

        

