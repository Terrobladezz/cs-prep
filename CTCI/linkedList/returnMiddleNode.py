from linkedListHelpers import traversal
from linkedListHelpers import generateLinkedList

head = generateLinkedList([1, 2, 3, 4, 5, 6, 7, 8, 9])

reqNode = head

while reqNode.value != 5:
        reqNode = reqNode.next

def deleteMidNode(node):
    node.value = node.next.value
    node.next = node.next.next

deleteMidNode(reqNode)
traversal(head)