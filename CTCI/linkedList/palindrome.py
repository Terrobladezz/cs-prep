from linkedListHelpers import generateLinkedList, traversal

inp  = 'RaceCar'
arrInp = []
for i in inp:
    arrInp.append(i.lower())
    
head = generateLinkedList(arrInp)

# traversal(head)


def isPalindrome(head):
    stack = []
    checkSTR = ''
    while head:
        checkSTR += head.value
        stack.append(head.value)
        head = head.next
    for char in checkSTR:
        if char != stack.pop():
            return False
    return True

    
print(isPalindrome(head))