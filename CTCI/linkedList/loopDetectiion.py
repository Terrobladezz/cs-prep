from linkedListHelpers import generateLinkedList, traversal

l1 = generateLinkedList([1, 2, 3, 4, 5, 6, 7, 8, 9])

temp = l1.next.next
temp2 = l1.next.next.next.next.next
temp2.next = temp


def detectLoop(head):
    dct = {}

    while head:
        if head in dct:
            return head
        else:
            dct[head] = None
        head = head.next
    return None

def detectLoop(head):
    dct = {}

    while head:
        if head in dct:
            return head
        else:
            dct[head] = None
        head = head.next
    return None


print(detectLoop(l1))