from linkedListHelpers import Node
from linkedListHelpers import traversal
from linkedListHelpers import generateLinkedList

head = generateLinkedList([1, 2, 3, 4, 5, 6, 7, 8, 9])


def deleteNode(root, value):

    if root.value == value:
        print("FOUND")
        return root.next

    ptr1 = root.next
    ptr2 = root

    while ptr1:
        if ptr1.value == value:
            ptr2.next = ptr1.next
            print("FOUND")
            return root
        else:
            ptr2 = ptr1
            ptr1 = ptr1.next


head = deleteNode(head,2)
traversal(head)



