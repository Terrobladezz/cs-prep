from linkedListHelpers import generateLinkedList

nodeList = [1, 2, 3, 4, 5, 6, 7, 8, 9]
k = 3

head = generateLinkedList(nodeList)


def returnKtoLast(head,k):
    ptr1 = head
    ptr2 = head
    dist = 0
    while ptr2:
        if dist < k:
            dist += 1
        else:
            ptr1 = ptr1.next
        ptr2 = ptr2.next
    return ptr1.value if ptr1 != head else None


print(returnKtoLast(head,1)) 
        

