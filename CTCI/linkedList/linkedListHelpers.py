class Node:
    def __init__(self, value = 0):
        self.value = value
        self.next = None


def traversal(root):
    while root:
        print(root.value)
        root = root.next


def generateLinkedList(arr):
    head = None
    n = None
    for node in range(len(arr)):
        if node == 0:
            n = Node(arr[node])
            head = n
        else:
            n.next = Node(arr[node])
            n = n.next
    return head


def deleteNode(root, value):

    if root.value == value:
        print("FOUND")
        return root.next

    ptr1 = root.next
    ptr2 = root

    while ptr1:
        if ptr1.value == value:
            ptr2.next = ptr1.next
            print("FOUND")
            return root
        else:
            ptr2 = ptr1
            ptr1 = ptr1.next
