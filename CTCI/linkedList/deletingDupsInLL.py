from linkedListHelpers import Node
from linkedListHelpers import traversal
from linkedListHelpers import generateLinkedList
def deleteDuplicates(root):
    ptr1 = root
    ptr2 = None
    uniqueList = []
    while ptr1:
        if ptr1.value in uniqueList:
            ptr1 = ptr1.next
            ptr2.next = ptr1
        else:
            uniqueList.append(ptr1.value)
            ptr2 = ptr1
            ptr1 = ptr1.next


head = generateLinkedList([1,7,7,7,2,5,9,0,1,2,4])

deleteDuplicates(head)
traversal(head)
