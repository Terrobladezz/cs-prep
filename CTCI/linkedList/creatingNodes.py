from linkedListHelpers import Node
from linkedListHelpers import traversal

nodeList = [1, 2, 3, 4, 5, 6, 7, 8, 9]

def generateLinkedList(arr):
    head = None
    n = None
    for node in range(len(arr)):
        if node == 0:
            n = Node(arr[node])
            head = n
        else:
            n.next = Node(arr[node])
            n = n.next
    return head



head = generateLinkedList(nodeList)
traversal(head)