from linkedListHelpers import generateLinkedList, traversal


def getLinkedListLength(head):
    count = 0
    while head:
        count += 1
        head = head.next
    return count


def sumListsReverse(l1, l2):
    multiplier = 1
    sum = 0
    rem = 0
    len1 = getLinkedListLength(l1)
    len2 = getLinkedListLength(l2)
    p1 = 0
    p2 = 0
    diff = len1 - len2
    if diff > 0:
        p2 = abs(diff)  # l2 is smaller
        node = l1
        node2 = l2
    else:
        p1 = abs(diff)  # l1 is smaller
        node = l2
        node2 = l1
    while node:
        temp = 0
        if node and node2:
            temp = node.value + node2.value
            node = node.next
            node2 = node2.next
        elif node:
            temp = node.value + 0
            node = node.next
        else:
            temp = node2.value + 0
            node2 = node2.next

        sum += temp * multiplier  # to maintain the ones, tenths and so on
        multiplier *= 10
    print(f'Sum : {sum}')


def sumList(l1,l2):
    s1 = []
    s2 = []
    multiplier = 1
    sum = 0
    while l1:
        s1.insert(0,l1.value)
        l1 = l1.next
    while l2:
        s2.insert(0,l2.value)
        l2 = l2.next

    diff = len(s1) - len(s2)
    if diff>0:
        for i in range(diff):
            s2.append(0)
    elif diff < 0:
        for i in range(abs(diff)):
            s1.append(0)

    for i in range(len(s1)):
        temp = multiplier * (s1[i] + s2[i])
        sum += temp
        multiplier *= 10
    
    print(sum)



l1 = generateLinkedList([7, 1, 6])
l2 = generateLinkedList([0, 5, 9, 2])

l3 = generateLinkedList([6 , 7, 8])
l4 = generateLinkedList([5, 4, 8])

# sumListsReverse(l1, l2)


sumList(l3,l4)

