from linkedListHelpers import generateLinkedList, traversal


l1 = generateLinkedList([1, 2, 3, 4, 5, 6, 7, 8, 9])
l2 = generateLinkedList([1,2,3,4,6,78,88,98])

# code to intersect two LL
ref5 = l1.next.next.next.next

temp = l2.next.next.next.next
ref5.next = temp


def isIntersecting(s1, s2):
    dct = {}
    while s1 or s2:
        if s1 in dct or s2 in dct:
            return True
        else:
            if s1:
                dct[s1] = None
                s1 = s1.next
            if s2:
                dct[s2] = None
                s2 = s2.next
    return False
        

print(isIntersecting(l1,l2))