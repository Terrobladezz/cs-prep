class Stack:
    def __init__(self):
        self.stack = []

    def push(self,item):
        self.stack.insert(0,item)


class StackOfPlates:
    def __init__(self, capacity):
        self.capacity = capacity
        self.stack = [Stack()]
        self.currStack = self.stack[-1]

    def push(self, item):
        if len(self.currStack.stack) == self.capacity:
            temp = Stack()
            temp.push(item)
            self.stack.append(temp)
            self.currStack = self.stack[-1]
        else:
            self.currStack.push(item)

    def pop(self):
        return self.currStack.stack.pop(0)

    def popAt(self,stackNumber): # for popping from one of the sub stacks
        if len(self.stack) < stackNumber:
            return False

        tempStack  = self.stack[stackNumber]
        tempValue =  tempStack.stack.pop(0)
        self.reArrange(stackNumber)
        return tempValue

    def reArrange(self,stackNumber):
            if stackNumber != len(self.stack) - 1:
                self.stack[stackNumber].push(self.stack[stackNumber + 1].stack[-1])
                self.stack[stackNumber + 1].stack.pop()
                self.reArrange(stackNumber+1)

    def showStackOfPlates(self):
        for item in self.stack:
            print(item.stack)


inpArr = [i for i in range(40)]

plates = StackOfPlates(10)

for item in inpArr:
    plates.push(item)

plates.pop()
print(f'Specific Stack {plates.popAt(1)}')

plates.showStackOfPlates()