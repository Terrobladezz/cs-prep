from Stack_helpers import Stack

stack = Stack()

test = [i for i in range(50)]
for i in test:
    stack.push(i)

stack.push(-1)

print("\n Commands:\n")
print(stack.peek())
print(stack.pop())
print(stack.pop())
print(stack.pop())

print(stack.isEmpty())

print(stack.minInStack())



