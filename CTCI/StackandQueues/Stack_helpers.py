class Stack:
    def __init__(self):
        self.stack = []
        self.min = None

    def pop(self):
        return self.stack.pop(0) if len(self.stack) else None

    def push(self,item):
        self.stack.insert(0,item)
        if not self.min or item < self.min :  # used for o(1) min value find
            self.min = item

    def peek(self):
        return self.stack[0] if len(self.stack) else None

    def isEmpty(self):
        return False if len(self.stack) else True

    def stackTraversal(self):
        for item in self.stack:
            print(item)

    def minInStack(self):
        return self.min