class Queue:
    def __init__(self):
        self.queue = []

    def add(self,item):
        self.queue.insert(0,item)

    def remove(self):
        return self.queue.pop() if len(self.queue) else None

    def peek(self):
        return self.queue[-1] if len(self.queue) else None


    def isEmpty(self):
        return False if len(self.queue) else True

    def queueTraversal(self):
        temp = self.queue
        while temp:
            print(temp.pop())