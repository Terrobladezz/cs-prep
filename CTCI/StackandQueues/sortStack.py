class Stack:
    def __init__(self):
        self.stack = []

    def push(self,item):
        self.stack.insert(0,item)
    
    def pop(self):
        return self.stack.pop(0) if len(self.stack) else None
    
    def peek(self):
        return self.stack[0] if len(self.stack) else None

    def isEmpty(self):
        return len(self.stack)

    def sortStack(self):
        tempStack = Stack()
        tempStack.push(self.pop())
        while len(self.stack):
            temp = self.peek()
            temp2 = tempStack.peek()
            if temp <= temp2:
                tempStack.push(self.pop())
            else:
                temp = self.pop()
                while temp2 < temp and len(tempStack.stack):
                    temp2 = tempStack.pop()
                    self.push(temp2)
                tempStack.push(temp)
        return tempStack.stack

import random

myStack = Stack()

inpData = [random.randint(1,21) for i in range(20)]

for item in inpData:
    myStack.push(item)
print(myStack.sortStack())