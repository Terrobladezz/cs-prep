data = [[0, 1], [0, 4], [1, 0], [1, 4], [1, 3], [1, 2], [2, 1],
        [2, 3], [3, 1], [3, 2], [3, 4], [4, 0], [4, 3], [4, 1]]
cs2 = [[1, 0], [2, 0], [3, 1], [3, 2], [2, 4]]
k = 5
adjMatrix = [[0 for i in range(k)] for j in range(k)]
adjList = [[i] for i in range(k)]
for node in cs2:
    adjMatrix[node[0]][node[1]] = 1
    # adjList[node[0]].append(node[1])
outArr = []
for i in range(k):
    sum = 0
    for j in range(k):
        sum += adjMatrix[i][j]
    if not sum:
        outArr.append(i)


print(outArr)
print(adjMatrix)

for i in range(k):
    if i not in outArr :
        for j in range(k):
            if adjMatrix[i][j] == 1 and j not in outArr:
                print("NOT POSSIBLE")
                break
        outArr.append(i)
        
            

def findOrder(numCourses, prerequisites):
    k = numCourses
    adjMatrix = [[0 for i in range(k)] for j in range(k)]
    for node in prerequisites:
        adjMatrix[node[0]][node[1]] = 1
    outArr = []
    for i in range(k):
        sum = 0
        for j in range(k):
            sum += adjMatrix[i][j]
        if not sum:
            outArr.append(i)
    x = k
    while x:
        for i in range(k):
            if i not in outArr :
                temp = False
                for j in range(k):
                    if adjMatrix[i][j] == 1 and j not in outArr:
                        temp = True
                if not temp:
                    outArr.append(i)
        x-=1
    if len(outArr) != k:
        return []
    return outArr

print(findOrder(3,[[0,1],[0,2],[1,2]]))
print(findOrder(4, [[1,0],[2,0],[3,1],[3,2]]))
