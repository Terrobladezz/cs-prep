def topKFrequent( nums, k):
    numDict  = {}
    outArr = []
    for num in nums:
        if num in numDict:
            numDict[num] += 1
        else:
            numDict[num] = 1
    while len(outArr) < k:
        Keymax = max(numDict, key= lambda x: numDict[x]) 
        numDict.pop(Keymax,None)
        outArr.append(Keymax)

    return outArr 


print(topKFrequent([3,2,2,1,1,1],2))