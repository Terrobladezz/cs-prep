input = "A man, a plan, a canal: Panama"


def isPalindrome(s):
    cleanedInput = ""
    for i in s:
        if i.isalnum():
            if i.isalpha():
                cleanedInput += i.lower()
            else:
                cleanedInput += i

    if len(cleanedInput) == 0 or len(cleanedInput) == 1:
        return True
    i = 0
    j = len(cleanedInput) - 1
    while i < j:
        if cleanedInput[i] != cleanedInput[j]:
            return False
        i += 1
        j -= 1
    return True


print(isPalindrome(input))
