# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


def removeElements(head, val):
    ptr = None
    ptr1 = head
    mainHead = None
    while ptr1:
        if ptr1.val != val:
            if ptr == None:
                ptr = ptr1
                mainHead = ptr
            else:
                ptr.next = ptr1
                ptr = ptr.next
        elif ptr != None:
            ptr.next = None
        ptr1 = ptr1.next


    return mainHead

def printLL(head):
    while head:
        print(head.val)
        head = head.next

#  1->2->6->3->4->5->6


six = ListNode(6, None)
five = ListNode(5, six)
four = ListNode(4, five)
three = ListNode(3, four)
six = ListNode(6, three)
two = ListNode(2, six)
head = ListNode(1, two)

head = removeElements(head,6)
printLL(head)

