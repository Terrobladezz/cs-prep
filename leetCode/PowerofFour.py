def isPowerOfFour(num):
    if num <= 0:
        return False

    while num > 1:
        if num % 4 != 0:
            return False
        num = int(num/4)
    return True


# print(isPowerOfFour(16))
print(isPowerOfFour(-64))
