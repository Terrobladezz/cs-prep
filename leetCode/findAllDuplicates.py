# def findDuplicates(nums):
#     dist = {}
#     duplicates = []
#     for num in nums:
#         if num in dist:
#             duplicates.append(num)
#         else:
#             dist[num] = None
#     return duplicates

# def findDuplicates(nums): #my best
#     outArr = [0 for i in range(len(nums))]
#     for num in nums:
#         outArr[num - 1] += 1

#     returnArr = []
#     for num in range(len(outArr)):
#         if outArr[num] == 2:
#             returnArr.append(num + 1)
#     print(returnArr)

def findDuplicates(nums): #nick white
    outArr = []
    for i in range(len(nums)):
        index = abs(nums[i]) - 1
        if nums[index] < 0:
            outArr.append(index + 1)
        else:
            nums[index] = -nums[index]
    print(outArr)

nums = [4,3,2,7,8,2,3,1]

findDuplicates(nums)