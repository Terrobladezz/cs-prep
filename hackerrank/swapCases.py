def swap_case(s):
    for i in range(len(s)) : 
        if s[i].islower():
            s[i] = s[i].toUpperCase()
        else : 
            s[i] = s[i] ^ 0x20
    return s

print(swap_case('HackerRank.com presents "Pythonist 2".'))