arr= [7,1,3,2,4,5,6]
# arr = [4 ,3 ,1 ,2]

def minimumSwaps(arr):
    length = len(arr)

    lookUp = {}
    count = 1
    for i in arr:
        lookUp[i] = count
        count += 1

    count = 0 
    for i in range(length):
        if arr[i] != i+1:
            temp = arr[i]
            arr[i], arr[lookUp[i+1] -1] = arr[lookUp[i+1] -1], arr[i]
            lookUp[temp] = lookUp[i+1]
            lookUp[i+1] = i+1
            count +=1
    return count

    


print(minimumSwaps(arr))
