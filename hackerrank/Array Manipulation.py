
def arrayManipulation(n, queries):
    arr = [0 for i in range(1,n)]
    for item in queries:
        query = item
        for i in range(query[0]-1,query[1]):
            arr[i] += query[2]

    max = -1
    for i in arr:
        if i > max : 
            max = i
            
    return max


print(arrayManipulation(10,[
[2, 6, 8],
[3, 5, 7],
[1, 8, 1],
[5 ,9 ,15]

]))