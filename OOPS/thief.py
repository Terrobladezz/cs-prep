def checkMagazine(magazine, note):
    magazineDict = {}
    noteDict = {}
    for item in magazine :
        if item in magazineDict:
            magazineDict[item] +=1
        else:
            magazineDict[item] = 1
    
    for item in note:
        if item in noteDict:
            noteDict[item] +=1
        else:
            noteDict[item] = 1
    
    for item in note :
        if item not in magazineDict:
            print('No')
            return
        if item in magazineDict:
            if magazineDict[item] >= 1:
                magazineDict[item] -=1
            else:
                print('No')
                return
    print('Yes')
    return


magazine = ['give', 'me', 'one', 'grand', 'today' ,'night']
note = ['give', 'one', 'grand', 'today']
checkMagazine(magazine,note)
