class Node:
    def __init__(self,value):
        self.left = None
        self.right = None
        self.value = value

head = Node(10)
second = Node(5)
second.left = Node(3)
second.right = Node(8)

head.left = second

third = Node(13)
fourth = Node(16)
head.right = third
# def showProperly(root):
#     if root:
#         showProperly(root.left)
#         print(root.value)
#         showProperly(root.right)

def ifPresent(root,key):
    if root:
        if root.value == key.value :
            print('Present')
            return
        if root.value < key.value :
            ifPresent(root.right,key)
        elif root.value > key.value:
            ifPresent(root.left,key)

        print('Not found')
        return None

def show(root):
    if root :
        print(root.value)
        show(root.left)
        show(root.right)

show(head)
ifPresent(head,fourth)