def sortedSquares(A):
    if A[0] > 0:
        A = [x**2 for x in A]
        return A
    else:
        b = [0] * len(A)
        first = 0
        last = len(A)-1
        bIndex = len(b)-1
        while first <= last:
            if (abs(A[first]) >= abs(A[last])):
                b[bIndex] = A[first]*A[first]
                first += 1
            else:
                b[bIndex] = A[last]*A[last]
                last -= 1
            bIndex -= 1
        return b


print(sortedSquares([-4,-1,0,3,10]))