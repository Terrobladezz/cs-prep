class TreeNode:
     def __init__(self, val=0, left=None, right=None):
         self.val = val
         self.left = left
         self.right = right



h1 = TreeNode(1,None,TreeNode(2,TreeNode(3,None,None),None))
h2 = TreeNode(2,TreeNode(1,None,TreeNode(4,None,None)),TreeNode(3,None,None))

def preorderTraversal(root) :
    arr = [root]
    outArr = []
    while len(arr) >0 :
        node = arr.pop()
        if node:
            outArr.append(node.val)
            if node.right :
                arr.append(node.right)
            if node.left:
                arr.append(node.left)
    return outArr



print(preorderTraversal(h2))
