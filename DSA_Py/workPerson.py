class Person:
    def __init__(self, name):
        self.name = name
    def work(self):
        raise NotImplementedError


class Teacher(Person) :
    def __init__(self,name,subject):
        super().__init__(name)
        self.subject = subject
    def work(self):
        print('I teach ',self.subject)

    
anirudh = Person('Anirudh')
Prerana = Teacher('Prerana','Maths')

print(anirudh.name,Prerana.name,Prerana.subject,anirudh.work())
