class Graph:
    def __init__(self, V):
        self.v = V
        self.matrix = [[0 for i in range(self.v)] for j in range(self.v)]

    def addEdge(self,u,v):
        self.matrix[u][v] = 1

    def printMatrix(self):
        for i in self.matrix:
            print(i)

    def BFS(self, root):
        queue = []
        visited = [False for i in range(self.v)]

        queue.append(root)

        while queue:
            s = queue.pop(0)

            if visited[s] == False:
                print(s, end= " ")
                visited[s]= True
                for i in range(self.v):
                    if self.matrix[s][i] and not visited[i]:
                        queue.append(i)

    def shortestPath(self,source,dest):

        dist = [-1 for i in range(self.v)]
        dist[source] = 0

        queue = []
        queue.append(source)

        visited = [False for i in range(self.v)]

        seq = [-1 for i in range(self.v)]

        seq[0] = source
        while queue:
            s = queue.pop(0)

            # print(s, end = " ")
            visited[s] = True
            if s == dest:
                sp = dist[s]
                outArr = []
                outArr.insert(0,dest)
                while sp:
                    outArr.insert(0,seq[s])
                    s = seq[s]
                    sp -=1
                print(f'Distance between source {source} and Destination {dest} is {dist[s]}, path : {outArr} \n')


            for i in range(self.v):
                if self.matrix[s][i] and not visited[i]:
                    dist[i] = dist[s] + 1
                    queue.append(i)
                    visited[i] = True
                    seq[i] = s


        print("Destination does not exist in graph")
        return -1



g = Graph(8)
g.addEdge(0, 1)
g.addEdge(0, 3)
g.addEdge(1,2)
g.addEdge(3,4)
g.addEdge(3,7)
g.addEdge(4,5)
g.addEdge(4,6)
g.addEdge(4,7)
g.addEdge(5,6)
g.addEdge(6,7)

for i in range(8):

    g.shortestPath(0,i)











# g.addEdge(0, 1)
# g.addEdge(0, 2)
# g.addEdge(1, 2)
# g.addEdge(2, 0)
# g.addEdge(2, 3)
# g.addEdge(3, 3)




# addEdge(adj, 0, 1);
#         addEdge(adj, 0, 3);
#         addEdge(adj, 1, 2);
#         addEdge(adj, 3, 4);
#         addEdge(adj, 3, 7);
#         addEdge(adj, 4, 5);
#         addEdge(adj, 4, 6);
#         addEdge(adj, 4, 7);
#         addEdge(adj, 5, 6);
#         addEdge(adj, 6, 7);

# g.printMatrix()
# g.BFS(2)
