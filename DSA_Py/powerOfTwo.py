def isPowerOfTwo(n):
    return True if (n & (n-1)) == 0 and n>0 else False


print(isPowerOfTwo(0))