a = [1,3,2,5,6,7,1]
swapped = True
while(swapped):
    swapped = False
    for i in range(0,len(a)-1):
        if(a[i] > a[i+1]):
            a[i+1],a[i] = a[i],a[i+1]
            swapped = True

print(a)            