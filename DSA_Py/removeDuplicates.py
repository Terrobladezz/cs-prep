def removeDuplicates(nums):
    i = 1
    count = 0
    origCounter = -1
    while i<len(nums):
        origCounter = i
        if nums[i] == nums[i-1]:
            count+=1
            i+=1
        else:
            if count == 0:
                i+=1
            else:
                nums = nums[:origCounter] + nums[count+origCounter:]
                count = 0
                i = origCounter
    print(nums)

removeDuplicates([0,0,1,1,1,2,2,3,3,4])