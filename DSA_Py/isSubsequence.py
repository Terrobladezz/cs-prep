def isSubsequence(s, t):
    pv = 0
    for i in s:
        index = t.find(i, pv, len(t))
        if index >= 0:
                pv = index
                t = t.replace(i, '', 1)
        else:
            return False
    return True

print(isSubsequence("axe", "ahgbdc"))
