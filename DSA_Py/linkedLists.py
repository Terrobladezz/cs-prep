class Node:
    def __init__(self, data):
        self.data = data
        self.next = None


class LinkedList:
    def __init__(self):
        self.head = None

    def showList(self):
        temp = self.head
        while temp:
            print(temp.data, '-->', end=" ")
            temp = temp.next

    def countNodes(self):
        count = 0
        temp = self.head
        while temp:
            count += 1
            temp = temp.next
        print("count", count)

    def deletNode(self, node):
        ptr1 = self.head
        prevNode = None
        while ptr1.data != node.data:
            prevNode = ptr1
            if ptr1.next != None:
                ptr1 = ptr1.next
        if ptr1.data == node.data and ptr1.next == node.next:
            prevNode.next = ptr1.next
        return


list = LinkedList()

list.head = Node(1)

second = Node(2)
third = Node(3)
five = Node(5)
six = Node(6)
second.next = third
third.next = five
five.next = six

list.head.next = second
second.next = third
list.head.next = second


def printLinkedList(head):
    while head:
        print(head.data)
        head = head.next


def insertNodeAtTail(head, data):
    mainHead = head
    while head:
        if head.next:
            head = head.next
        else:
            break

    temp = Node(data)
    if head:
        head.next = temp
    else:
        head = temp
        mainHead = head
    
    return mainHead

def insertNodeAtHead(head, data):
    node = Node(data)
    node.next = head
    return node

def compare_lists(llist1, llist2):
    while llist1 and llist2:
        if llist1.data != llist2.data:
            return 0
        llist1 = llist1.next
        llist2 = llist2.next
    if llist1 == llist2 == None:
        return 1
    return 0


insertNodeAtTail(list.head,7)
printLinkedList(list.head)
