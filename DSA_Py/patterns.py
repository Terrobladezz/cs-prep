def pattern(n):
    i = 1
    j = 1
    while i<=n:
        iterator = j
        while iterator:
            print(i, end = ' ')
            i+=1
            iterator-=1
        print('\n')
        j+=1

pattern(10)