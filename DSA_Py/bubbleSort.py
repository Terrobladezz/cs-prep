def bubbleSort(arr):
    flag = True
    while  flag  :
        flag = False
        for i in range(len(arr)-1):
            if arr[i] > arr[i+1]:
                arr[i],arr[i+1] = arr[i+1],arr[i]
                flag = True
    return arr
            

print(bubbleSort([64, 34, 25, 12, 22, 11, 90]))
