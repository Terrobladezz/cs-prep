class CharNode:
    def __init__(self):
        self.end = False
        self.children = {}

class Trie:
    def __init__(self):
        self.root = CharNode()

    def addWord(self,word):
        curr = self.root
        for char in word:
            node = curr.children.get(char)
            if not node:
                node = CharNode()
                curr.children[char] = node
            curr = node
        curr.end = True

    def search(self,word):
        curr = self.root
        for i in range(len(word)):
            if word[i].isalpha():
                node = curr.children.get(word[i])
                if not node:
                    return False
                curr = node
            else:
                if i == len(word):
                    return True
                else:
                    if not len(curr.children):
                        return False
                temp = next(iter(curr.children))
                curr = curr.children[temp]
        return curr.end


trie = Trie()
trie.addWord('Bar')
print(trie.search('*ar'))
