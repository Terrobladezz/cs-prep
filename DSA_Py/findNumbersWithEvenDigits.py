def findNumbers(nums):
    def getDigitsSize(num: int) -> int:
        count = 0
        while num > 0:
            num = int(num/10)
            count += 1
        return count
    evens = 0
    for num in nums:
        size = getDigitsSize(num)
        if size % 2 == 0:
            evens += 1
    return evens


print(findNumbers([12,345,2,6,1771]))