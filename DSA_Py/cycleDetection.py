def has_cycle(head):
    paths = {}

    while head:
        
        if head in paths.keys():
            return True
        else:
            paths[head] = True
            head = head.next
    return False
        


has_cycle(list.head)