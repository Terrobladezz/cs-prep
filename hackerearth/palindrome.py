def palindrome(word):
    p1 = 0
    p2 = len(word)-1
    while p2>p1 : 
        if word[p1] != word[p2]:
            print("NO")
            return 
        p1+=1
        p2-=1
    if len(word)%2==0:
        print("YES EVEN")
    else:
        print ("YES ODD") 
    return

palindrome('abc')
palindrome('abba')
palindrome('aba')
