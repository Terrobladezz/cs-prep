n = int(input())
a = input()


def splitHouses(string):
    for i in range(len(string)-1):
        if string[i] == string[i+1] == 'H':
            return [False]
    return [True, string.replace('.','B')]
    
        
    
result = splitHouses(a)
if result[0]:
    print('YES')
    print(result[1])
else:
    print('NO')


# TIME COMPLEXITY IS 0(n)