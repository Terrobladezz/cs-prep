# https: // www.hackerearth.com/problem/algorithm/f-counting-2d8b6288/

def isSequence(num):
    if num ==1 :
        return a
    if num == 2:
        return b
    return isSequence(num-1) + isSequence(num-2) 


a,b = map(int,input().split())
q = int(input())

while q>0 :
    x = int(input())
    index = 1
    num =0
    found = False
    if a > x and b >x:
        print('NO')
    else:

        while(num <= x):
            num = isSequence(index)
            if num == x :
                found = True
                break
            index+=1
        if found:
            print('YES')
        else:
            print('NO')    
          
    
    q-=1
        
            
