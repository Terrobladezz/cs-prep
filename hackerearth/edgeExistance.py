N,M = [int(x) for x in input().split()]

adjMatrix = [[0]*N for i in range(N)]

while M:
    a,b = [int(x) for x in input().split()]
    adjMatrix[a][b] = 1
    adjMatrix[b][a] = 1
    M -=1

q = int(input())

while q :
    a,b = [int(x) for x in input().split()]
    if adjMatrix[a][b]:
        print("YES")
    else:
        print("NO")
    q-=1